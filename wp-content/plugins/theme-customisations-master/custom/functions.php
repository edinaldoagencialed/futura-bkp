<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */


function storefront_credit() {
        ?>
        <style>
    @media only screen and (max-width: 425px){
        .img-footer{
            display: none;
        }
    }
</style>

<section class="site-info" style="padding-top: 0px;">
          <img class="img-footer" src="https://futurapreligas.com.br/wp-content/uploads/2019/01/RODAPE.png" alt="">
        </section>
        <section class="site-credits">
        	<p style="font-size: 12px; color: #76787e !important; text-align: center">
        		Todos os direitos reservados à Futura Pré-Ligas<br>
        		Desenvolvido por <a style="color: #ff8000 !important;" href="https://agencialed.com.br/" target="_blank">LED DIGITAL</a>.
        	</p>
        </section>

        <?php
   }

   function remove_hook() {
        remove_action( 'homepage', 'storefront_product_categories', 20);
   } 

   add_action( 'homepage', 'remove_hook');



   function remove_secondary_navigation () {
      remove_action( 'storefront_header', 'storefront_product_search' );
   }

   add_action('storefront_header', 'remove_secondary_navigation');

  
    function verifica_id_post() { 
      $show_form = true;
      $args =  array(6, 7, 8, 502);
      $page = get_queried_object_id();
      for ($i=0; $i < sizeof($args); $i++) {
        if ($page == $args[$i]) {
          $show_form = false;
        }
      }
      
      if ($show_form == true) {
        add_action('storefront_before_footer', 'teste_footer');
        return true;
      } 
      else {
        return false;
      }
    }

    add_action('storefront_header', 'verifica_id_post');
     add_action('storefront_header', 'adiciona_font_awesome');
    //add_action('storefront_before_footer', 'teste_footer');

    function adiciona_font_awesome() {
      ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>
        <style> 
          a { text-decoration: none !important; }
      </style>
      <?php 
    }

    function teste_footer() { 
      ?>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
      <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>

      <style>
        .formulario-contato { background-color: #efefef; margin-top: 15px; padding: 35px 0px;}
        .formulario-contato button { 
          background-color: #ff8000 !important;
          color: #ffffff !important;
          float: right !important;
          margin-bottom: 20px !important; 
        }
        .texto-form { margin-top: 3% }
        .text-form {  color: #6d6d6d; margin-bottom: 0px; }
        .texto-form p { color: #6d6d6d; margin-left: 20%; }
         .texto-form a:hover { color: #ff8000 }
        .texto-form a { text-decoration: none; color: #6d6d6d}
        .chamada-form { color: #ff8000; text-align: center; font-weight: bold; }
        .formulario { margin-top: 3%; }

        @media only screen and (max-width: 720px) {
			.texto-form { margin-top: 3%; }
        	.texto-form p { color: #6d6d6d; margin-left: 0%; }
        	.texto-form a { text-decoration: none; }
        	.formulario-contato { margin-top: 15px; }
		}	
      </style>

        <section class="formulario-contato">
            <div class="container">
               <h2 class="chamada-form">Fale Conosco</h2>
               <p class="text-form">Preencha os dados abaixo que entraremos em contato o mais breve possível. <br> Suas dúvidas e opiniões são muito importante para nós.</p>
                <div class="row">      
                  <div class="formulario col-12 col-md-6 text-left">
                    <?php echo do_shortcode('[wpforms id="433" title="false" description="false"]'); ?>
                  </div>
                  <div class="texto-form col-12 col-md-6">
                  <p><i class="fas fa-envelope"></i><b> E-mail:</b><br><a href="mailto:contato@futurapreligas.com.br">contato@futurapreligas.com.br</a>
                  <p><i class="fas fa-phone"></i><b> Telefone: </b><br><a href="tel:(17)32298624">17 3229 8624</a></p>
                  <p><i class="fab fa-whatsapp"></i><b> WhatsApp: </b><br><a href="https://wa.me/5517996275332?text=Entrar%20em%20contato" target="_blanc">17 99627 5332</a></p></p>
                  <p><i class="fas fa-map-marker-alt"></i><b> Endereço: </b><br> R. Jaír Martins Mil Homens, 500 - 1013 - Vila São José, São José do Rio Preto - SP, 15090-080</a></p>
                  </div>
                </div>
            </div>
        </section>
      <?php 
   }





