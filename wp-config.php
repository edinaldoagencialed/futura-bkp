<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home/futurapreligas/public_html/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'futurapr_dbsite');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'futurapr_dbuser');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'eS6E8+4vC9dC');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost:3306');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7vd:f~/Cq:<AR8T{WW,L/U=kYJgMx-b}!YB5.F7h+bgksM.)?90xIL%>au%(*rv8');
define('SECURE_AUTH_KEY',  '(pUZI+BYx?AU0)ug4qgs3YiiPz=5QvLWazQ*qPA%pmMsu$cMnC.DsFM*hP/97V[`');
define('LOGGED_IN_KEY',    'Jm]I<JqcB?6GJ*lKupgi_0#W_<4a/%7qK<:.z!aDZEh3n=PfT}w^)1QleY7,y*dm');
define('NONCE_KEY',        'WnRBUHc|9-9v2YmBn2O1kuub,mRbD-U-HrSlsb_hSj8?8%m.NMYCxlc1a~wY=<Hq');
define('AUTH_SALT',        '5$B1lG2(D0i}y%:g*HOIrEB>89yYBfF4.3Ea/V|Ddhq8#yLr{K=eOb2M(kl@LJ}g');
define('SECURE_AUTH_SALT', 'BRMMZSRW2F10F|hy14[JHnihuI7iG,`Y8cn[?mo#9oR[-v?YnZ1V?6@YQvn2u^#h');
define('LOGGED_IN_SALT',   'QNquKx_F,4_3.rr?{ uq3DpoTW76KhrR_~#_Z/QZV/MDJvci`#eH$l,gEVvQ,>jV');
define('NONCE_SALT',       'CaHtgWg#v3O:`{1P;g2_pY/wA$z).a8!a~H0Fj2tn>yxN#ML|TxGcCG_SgvmWKkq');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
